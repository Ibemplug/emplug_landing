FROM bitnami/node:latest
#LABEL MAINTAINER "DVLabs <darren.willy@gmail.com>"

#Show colors in docker terminal
ENV COMPOSE_HTTP_TIMEOUT=50000
ENV TERM="xterm-256color"
ENV workdir="/app"

RUN ["mkdir", "/install"]

ADD ["./package.json", "/install"]

WORKDIR "/install"

RUN npm install -g yarn
RUN yarn install

ENV NODE_PATH=/install/node_modules

COPY . /app/

WORKDIR $workdir

EXPOSE 3000