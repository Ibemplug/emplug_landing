import React, { useState } from 'react';
import { HashRouter, Route, Switch} from "react-router-dom";
import ScrollToTop from "react-router-scroll-top";

// Components
import Footer from "./Components/Footer/footer";
import Home from "./Components/Pages/home";
import Register from "./Components/pages/register";
import About from "./Components/pages/about";
import Career from "./Components/pages/careers";
import Events from "./Components/pages/events";
import CookiePolicy from "./Components/pages/cookie_policy";
import PrivacyPolicy from "./Components/pages/privacy_policy";
import Programs from "./Components/pages/programs";
import Team from "./Components/pages/team";
import Student from "./Components/Pages/student";
import Intern from "./Components/Pages/intern";
import TOS from "./Components/pages/tos";
import NotFound from "./Components/pages/NotFound";
import Table from "./Components/Pages/table";

// CSS
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "./css/style.css";
import "./App.scss";

// JS
import "./js/full-script.js";


function App() {
  const [loader, setLoader] = useState(()=> {
    return true;
  })
  return (
    <div>     
    <HashRouter>
    <ScrollToTop>
      <Switch>
        <Route exact path="/" name="Home" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/register" component={Register} />
        <Route path="/programs" component={Programs} />
        <Route path="/careers" component={Career} />
        <Route path="/events" component={Events} />
        <Route path="/student" component={Student} />
        <Route path="/intern" component={Intern} />
        <Route path="/cp" component={CookiePolicy} />
        <Route path="/pp" component={PrivacyPolicy} />
        <Route path="/team" component={Team} />
        <Route path="/table" component={Table} />
        <Route path="/tos" component={TOS} />
        <Route component={NotFound} />
      </Switch>
    </ScrollToTop>
  </HashRouter>
  <Footer />
    </div>
  );
}

export default App;
