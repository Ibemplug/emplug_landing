import React from "react";
import { Link } from "react-router-dom";

import {
  ValidationForm,
  TextInput,
  SelectGroup,
  Checkbox
} from "react-bootstrap4-form-validation";
import validator from "validator";

// Imports
import Logo from "../../images/logo.png";
import CompanyLogo2 from "../../images/company-logo2.png";
import CompanyLogo from "../../images/company-logo.png";
import AdCard from "../../images/adcard.png";
import EmCard from "../../images/emcard.png";
import JobCard from "../../images/jobcard.png";
import Coming from "../../images/comingsoon.png";
import Compare from "../../images/compare-after.png";
import Sms from "../../images/envelope1.png";
import io from "socket.io-client";

// Axios
import axios from "axios";

var img = [CompanyLogo2, CompanyLogo];
var link = "<span><a href='#'>Apply</a> | <a href='#'>Refer</a></span>";
const Socket = () => {
  const socket = io("http://207.154.228.170:9090/liveticker");

  socket.on("message", msg => {
    function timeDifference(current, previous) {
      var msPerMinute = 60 * 1000;
      var msPerHour = msPerMinute * 60;
      var msPerDay = msPerHour * 24;
      var msPerMonth = msPerDay * 30;
      var msPerYear = msPerDay * 365;

      var elapsed = previous - current;

      if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + " seconds ago";
      } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + " minutes ago";
      } else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + " hours ago";
      } else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + " days ago";
      } else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + " months ago";
      } else {
        return +Math.round(elapsed / msPerYear) + " years ago";
      }
    }

    // timeDifference(date,d)
    //=============================================>
    //array0

    var data = JSON.parse(msg);
    console.log(data.message);
    var table = document.getElementById("myTable");
    var tr = document.getElementsByTagName("tr");
    function timeDifference(current, previous) {
      var msPerMinute = 60 * 1000;
      var msPerHour = msPerMinute * 60;
      var msPerDay = msPerHour * 24;
      var msPerMonth = msPerDay * 30;
      var msPerYear = msPerDay * 365;

      var elapsed = previous - current;

      if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + " seconds ago";
      } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + " minutes ago";
      } else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + " hours ago";
      } else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + " days ago";
      } else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + " months ago";
      } else {
        return +Math.round(elapsed / msPerYear) + " years ago";
      }
    }
    function myFunc() {
      for (var i = 0; i < data.message.length; i++) {
        var img1 = document.createElement("img");
        var row = table.insertRow(i);
        var cella = row.insertCell(0);
        img1.src = `${Sms}`;
        img1.style.height = "25px";
        console.log(img1);
        cella.appendChild(img1);
        // cella. = `image logo here`;
        var cella = row.insertCell(1);
        cella.innerText = `${data.message[i].profession} is needed`;
        var cellb = row.insertCell(2);
        cellb.innerText = data.message[i].sector;
        var cellc = row.insertCell(3);
        cellc.innerText = data.message[i].location;
        var celld = row.insertCell(4);
        var date = new Date(data.message[i].timestamp);
        var d = new Date();
        celld.innerText = `${timeDifference(date, d)}`;
      }
      tr[1].className = "biege";
      tr[2].className = "two";
      tr[3].className = "biege";
      tr[4].className = "two";
      tr[5].className = "biege";
      tr[6].className = "two";
      tr[7].className = "biege";
      tr[8].className = "two";
      tr[9].className = "biege";
      table.deleteRow(-1);
    }
    myFunc();
  });
};

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      change: "login",
      socket_change: "",
      calling_code: "",
      firstname: "",
      lastname: "",
      country: "",
      sms_verify_phone: "",
      email: "",
      password: "",
      password2: "",
      tos: false,
      error: null,
      message: "",
      redirectToReferrer: false
    };
    this.changeLogin = this.changeLogin.bind(this);

    this.onChange = this.onChange.bind(this);

    // this.handleSubmit = this.handleSubmit.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit = (e, formData, inputs) => {
    e.preventDefault();
    // alert(JSON.stringify(formData, null, 2));
    // fetch("emplug.com/users/register", {
    fetch("http://192.168.0.10:82/users/register", {
      body: JSON.stringify(this.state),
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "content-type": "application/json"
      },
      method: "POST",
      mode: "cors",
      redirect: "follow",
      referrer: "no-referrer"
    })
      .then(function(response) {
        return response.json();
        this.props.history.push("/");
        // console.log(response);
        // return
        // if (response.status === 200) {
        //   console.log(response);
        //   alert("Registration successful");
        // } else {
        //   alert("Issues saving");
        // }
      })
      .then(function(data) {
        console.log(data.message);
        alert(JSON.stringify(data.message));
        this.setState({
          message: JSON.parse(data.message),
          redirectToReferrer: true
        });
        this.props.history.push("/");
      });
  };

  handleErrorSubmit = (e, formData, errorInputs) => {
    console.log(errorsInputs);
  };

  matchPassword = value => {
    return value && value === this.state.password;
  };

  changeLogin = () => {
    this.setState({
      change: "register"
    });
  };

  changeBackToLogin = () => {
    this.setState({
      change: "login"
    });
  };

  myFunction = e => {
    this.setState({
      [e.target.name]: e.target.value,
      calling_code: e.target.value
    });
  };

  componentDidMount() {
    Socket();
  }

  render() {
    return (
      <div>
        <section className="homeHeader">
          <div className="container-fluid pt-3">
            <nav className="nav d-flex justify-content-between">
              <a
                to="https://www.emplug.com"
                className="navbar-brand d-flex align-items-center"
              >
                <img src={Logo} alt="emplug logo" />
              </a>
            </nav>
          </div>

          <div>{this.state.message}</div>

          <div className="headerContent">
            <div className="row">
              <div className="col-md-8 col-sm-12 leftRow">
                <h3>Welcome to Emplug</h3>
                <p className="mr-5">
                  Emplug enables everyone to plug to business, employment
                  opportunities, services and to connect with friends and
                  family. From chatting and socializing with family and friends
                  to getting jobs calls and alerts from employers, Emplug is a
                  trusted service community for you to create wealth, grow your
                  business or just have fun.
                </p>
              </div>

              <div className="col-md-4 col-sm-12">
                <div className="formRow">
                  {this.state.change === "login" ? (
                    <div className="formRow rounded container">
                      <div className="formTitle mb-0 ml-4">
                        <h3 className="pb-2">
                          <b>Login</b>
                        </h3>
                      </div>

                      <form
                        action="https://www.emplug.com/users/login"
                        method="post"
                      >
                        <div className="input-group form-group fomBorderless">
                          <div className="form-icon">
                            <span>
                              <i className="fas fa-envelope pr-4 pb-0" />
                            </span>
                          </div>
                          <input
                            type="text"
                            name="data[User][email]"
                            className="form-control"
                            placeholder="Email"
                          />
                          <input
                            type="hidden"
                            name="data[User][id]"
                            className="form-control"
                            value=""
                            placeholder="Email"
                          />
                          <input
                            type="hidden"
                            name="_method"
                            value="POST"
                            className="form-control"
                            placeholder="Email"
                          />
                        </div>
                        <div className="input-group form-group mb-0">
                          <div className="form-icon">
                            <span>
                              <i className="material-icons text-white pr-4 pb-0 pt-2">
                                vpn_key
                              </i>
                            </span>
                          </div>

                          <input
                            type="password"
                            name="data[User][password]"
                            className="form-control"
                            placeholder="Password"
                          />
                        </div>
                        <div className="form-check pt-4">
                          <div className="row">
                            <div className="col">
                              <small>
                                <input
                                  className="form-check-input"
                                  type="checkbox"
                                  value=""
                                  id="defaultCheck1"
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor="defaultCheck1"
                                >
                                  Remember Me
                                </label>
                              </small>
                            </div>
                            <div className="col ml-auto text-right">
                              <small>
                                <a href="https://www.emplug.com/users/recover">
                                  Forgot Password?
                                </a>
                              </small>
                            </div>
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="form-icon" />

                          <div className="text-right">
                            <input
                              type="submit"
                              value="LOGIN"
                              className="btn btn-lg login_btn"
                              style={{ width: "90%" }}
                            />
                          </div>
                        </div>
                      </form>

                      <div className="pt-2 text-right">
                        <div className="small container-fluid">
                          <div className=" d-flex justify-content-end">
                            <p>
                              Don't have an Account?{" "}
                              <a
                                href="#"
                                onClick={this.changeLogin}
                                className="text-light redirect"
                              >
                                Sign Up
                              </a>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : this.state.change === "register" ? (
                    <div className="sign_up px-3">
                      <h5 className="pt-2">
                        <span>GET STARTED- IT'S FREE</span>
                      </h5>
                      <hr className="get_started" />
                      <div className="mt-3">
                        <ValidationForm
                          onSubmit={this.handleSubmit}
                          onErrorSubmit={this.handleErrorSubmit}
                          method="POST"
                        >
                          <div className="row my-3">
                            <div className="col-6">
                              <TextInput
                                name="firstname"
                                id="firstname"
                                required
                                value={this.state.firstName}
                                onChange={this.onChange}
                                placeholder="First name"
                              />
                            </div>
                            <div className="col-6">
                              <TextInput
                                name="lastname"
                                id="lastname"
                                required
                                placeholder="Last name"
                                value={this.state.lastName}
                                onChange={this.onChange}
                              />
                            </div>
                          </div>
                          <div className="my-3">
                            <SelectGroup
                              name="country"
                              id="color"
                              value={this.state.country}
                              required
                              errorMessage="Please select a country."
                              onChange={this.onChange}
                              onChange={this.myFunction}
                            >
                              <option value="">Select your Country</option>
                              <option data-countrycode="DZ" value="+213">
                                Algeria{" "}
                              </option>
                              <option data-countrycode="AD" value="+376">
                                Andorra{" "}
                              </option>
                              <option data-countrycode="AO" value="+244">
                                Angola{" "}
                              </option>
                              <option data-countrycode="AI" value="+1264">
                                Anguilla{" "}
                              </option>
                              <option data-countrycode="AG" value="+1268">
                                Antigua &amp; Barbuda{" "}
                              </option>
                              <option data-countrycode="AR" value="+54">
                                Argentina
                              </option>
                              <option data-countrycode="AM" value="+374">
                                Armenia{" "}
                              </option>
                              <option data-countrycode="AW" value="+297">
                                Aruba{" "}
                              </option>
                              <option data-countrycode="AU" value="+61">
                                Australia
                              </option>
                              <option data-countrycode="AT" value="+43">
                                Austria
                              </option>
                              <option data-countrycode="AZ" value="+994">
                                Azerbaijan{" "}
                              </option>
                              <option data-countrycode="BS" value="+1242">
                                Bahamas{" "}
                              </option>
                              <option data-countrycode="BH" value="+973">
                                Bahrain{" "}
                              </option>
                              <option data-countrycode="BD" value="+880">
                                Bangladesh{" "}
                              </option>
                              <option data-countrycode="BB" value="+1246">
                                Barbados{" "}
                              </option>
                              <option data-countrycode="BY" value="+375">
                                Belarus{" "}
                              </option>
                              <option data-countrycode="BE" value="+32">
                                Belgium
                              </option>
                              <option data-countrycode="BZ" value="+501">
                                Belize{" "}
                              </option>
                              <option data-countrycode="BJ" value="+229">
                                Benin{" "}
                              </option>
                              <option data-countrycode="BM" value="+1441">
                                Bermuda{" "}
                              </option>
                              <option data-countrycode="BT" value="+975">
                                Bhutan{" "}
                              </option>
                              <option data-countrycode="BO" value="+591">
                                Bolivia{" "}
                              </option>
                              <option data-countrycode="BA" value="+387">
                                Bosnia Herzegovina{" "}
                              </option>
                              <option data-countrycode="BW" value="+267">
                                Botswana{" "}
                              </option>
                              <option data-countrycode="BR" value="+55">
                                Brazil{" "}
                              </option>
                              <option data-countrycode="BN" value="+673">
                                Brunei{" "}
                              </option>
                              <option data-countrycode="BG" value="+359">
                                Bulgaria{" "}
                              </option>
                              <option data-countrycode="BF" value="+226">
                                Burkina Faso{" "}
                              </option>
                              <option data-countrycode="BI" value="+257">
                                Burundi{" "}
                              </option>
                              <option data-countrycode="KH" value="+855">
                                Cambodia{" "}
                              </option>
                              <option data-countrycode="CM" value="+237">
                                Cameroon{" "}
                              </option>
                              <option data-countrycode="CA" value="+1">
                                Canada{" "}
                              </option>
                              <option data-countrycode="CV" value="+238">
                                Cape Verde Islands{" "}
                              </option>
                              <option data-countrycode="KY" value="+1345">
                                Cayman Islands{" "}
                              </option>
                              <option data-countrycode="CF" value="+236">
                                Central African Republic{" "}
                              </option>
                              <option data-countrycode="CL" value="+56">
                                Chile
                              </option>
                              <option data-countrycode="CN" value="+86">
                                China{" "}
                              </option>
                              <option data-countrycode="CO" value="+57">
                                Colombia{" "}
                              </option>
                              <option data-countrycode="KM" value="+269">
                                Comoros{" "}
                              </option>
                              <option data-countrycode="CG" value="+242">
                                Congo{" "}
                              </option>
                              <option data-countrycode="CK" value="+682">
                                Cook Islands{" "}
                              </option>
                              <option data-countrycode="CR" value="+506">
                                Costa Rica{" "}
                              </option>
                              <option data-countrycode="HR" value="+385">
                                Croatia{" "}
                              </option>
                              <option data-countrycode="CU" value="+53">
                                Cuba{" "}
                              </option>
                              <option data-countrycode="CY" value="+90392">
                                Cyprus North{" "}
                              </option>
                              <option data-countrycode="CY" value="+357">
                                Cyprus South{" "}
                              </option>
                              <option data-countrycode="CZ" value="+42">
                                Czech Republic{" "}
                              </option>
                              <option data-countrycode="DK" value="+45">
                                Denmark{" "}
                              </option>
                              <option data-countrycode="DJ" value="+253">
                                Djibouti{" "}
                              </option>
                              <option data-countrycode="DM" value="+1809">
                                Dominica{" "}
                              </option>
                              <option data-countrycode="DO" value="+1809">
                                Dominican Republic{" "}
                              </option>
                              <option data-countrycode="EC" value="+593">
                                Ecuador{" "}
                              </option>
                              <option data-countrycode="EG" value="+20">
                                Egypt{" "}
                              </option>
                              <option data-countrycode="SV" value="+503">
                                El Salvador{" "}
                              </option>
                              <option data-countrycode="GQ" value="+240">
                                Equatorial Guinea{" "}
                              </option>
                              <option data-countrycode="ER" value="+291">
                                Eritrea{" "}
                              </option>
                              <option data-countrycode="EE" value="+372">
                                Estonia{" "}
                              </option>
                              <option data-countrycode="ET" value="+251">
                                Ethiopia{" "}
                              </option>
                              <option data-countrycode="FK" value="+500">
                                Falkland Islands{" "}
                              </option>
                              <option data-countrycode="FO" value="+298">
                                Faroe Islands{" "}
                              </option>
                              <option data-countrycode="FJ" value="+679">
                                Fiji{" "}
                              </option>
                              <option data-countrycode="FI" value="+358">
                                Finland{" "}
                              </option>
                              <option data-countrycode="FR" value="+33">
                                France
                              </option>
                              <option data-countrycode="GF" value="+594">
                                French Guiana{" "}
                              </option>
                              <option data-countrycode="PF" value="+689">
                                French Polynesia{" "}
                              </option>
                              <option data-countrycode="GA" value="+241">
                                Gabon{" "}
                              </option>
                              <option data-countrycode="GM" value="+220">
                                Gambia{" "}
                              </option>
                              <option data-countrycode="GE" value="+7880">
                                Georgia{" "}
                              </option>
                              <option data-countrycode="DE" value="+49">
                                Germany
                              </option>
                              <option data-countrycode="GH" value="+233">
                                Ghana{" "}
                              </option>
                              <option data-countrycode="GI" value="+350">
                                Gibraltar{" "}
                              </option>
                              <option data-countrycode="GR" value="+30">
                                Greece
                              </option>
                              <option data-countrycode="GL" value="+299">
                                Greenland{" "}
                              </option>
                              <option data-countrycode="GD" value="+1473">
                                Grenada{" "}
                              </option>
                              <option data-countrycode="GP" value="+590">
                                Guadeloupe{" "}
                              </option>
                              <option data-countrycode="GU" value="+671">
                                Guam{" "}
                              </option>
                              <option data-countrycode="GT" value="+502">
                                Guatemala{" "}
                              </option>
                              <option data-countrycode="GN" value="+224">
                                Guinea{" "}
                              </option>
                              <option data-countrycode="GW" value="+245">
                                Guinea - Bissau{" "}
                              </option>
                              <option data-countrycode="GY" value="+592">
                                Guyana{" "}
                              </option>
                              <option data-countrycode="HT" value="+509">
                                Haiti{" "}
                              </option>
                              <option data-countrycode="HN" value="+504">
                                Honduras{" "}
                              </option>
                              <option data-countrycode="HK" value="+852">
                                Hong Kong{" "}
                              </option>
                              <option data-countrycode="HU" value="+36">
                                Hungary
                              </option>
                              <option data-countrycode="IS" value="+354">
                                Iceland{" "}
                              </option>
                              <option data-countrycode="IN" value="+91">
                                India
                              </option>
                              <option data-countrycode="ID" value="+62">
                                Indonesia
                              </option>
                              <option data-countrycode="IR" value="+98">
                                Iran
                              </option>
                              <option data-countrycode="IQ" value="+964">
                                Iraq{" "}
                              </option>
                              <option data-countrycode="IE" value="+353">
                                Ireland{" "}
                              </option>
                              <option data-countrycode="IL" value="+972">
                                Israel{" "}
                              </option>
                              <option data-countrycode="IT" value="+39">
                                Italy{" "}
                              </option>
                              <option data-countrycode="JM" value="+1876">
                                Jamaica{" "}
                              </option>
                              <option data-countrycode="JP" value="+81">
                                Japan
                              </option>
                              <option data-countrycode="JO" value="+962">
                                Jordan{" "}
                              </option>
                              <option data-countrycode="KZ" value="+7">
                                Kazakhstan
                              </option>
                              <option data-countrycode="KE" value="+254">
                                Kenya{" "}
                              </option>
                              <option data-countrycode="KI" value="+686">
                                Kiribati{" "}
                              </option>
                              <option data-countrycode="KP" value="+850">
                                Korea North{" "}
                              </option>
                              <option data-countrycode="KR" value="+82">
                                Korea South
                              </option>
                              <option data-countrycode="KW" value="+965">
                                Kuwait{" "}
                              </option>
                              <option data-countrycode="KG" value="+996">
                                Kyrgyzstan{" "}
                              </option>
                              <option data-countrycode="LA" value="+856">
                                Laos{" "}
                              </option>
                              <option data-countrycode="LV" value="+371">
                                Latvia{" "}
                              </option>
                              <option data-countrycode="LB" value="+961">
                                Lebanon{" "}
                              </option>
                              <option data-countrycode="LS" value="+266">
                                Lesotho{" "}
                              </option>
                              <option data-countrycode="LR" value="+231">
                                Liberia{" "}
                              </option>
                              <option data-countrycode="LY" value="+218">
                                Libya{" "}
                              </option>
                              <option data-countrycode="LI" value="+417">
                                Liechtenstein{" "}
                              </option>
                              <option data-countrycode="LT" value="+370">
                                Lithuania{" "}
                              </option>
                              <option data-countrycode="LU" value="+352">
                                Luxembourg{" "}
                              </option>
                              <option data-countrycode="MO" value="+853">
                                Macao{" "}
                              </option>
                              <option data-countrycode="MK" value="+389">
                                Macedonia{" "}
                              </option>
                              <option data-countrycode="MG" value="+261">
                                Madagascar{" "}
                              </option>
                              <option data-countrycode="MW" value="+265">
                                Malawi{" "}
                              </option>
                              <option data-countrycode="MY" value="+60">
                                Malaysia
                              </option>
                              <option data-countrycode="MV" value="+960">
                                Maldives{" "}
                              </option>
                              <option data-countrycode="ML" value="+223">
                                Mali{" "}
                              </option>
                              <option data-countrycode="MT" value="+356">
                                Malta{" "}
                              </option>
                              <option data-countrycode="MH" value="+692">
                                Marshall Islands{" "}
                              </option>
                              <option data-countrycode="MQ" value="+596">
                                Martinique{" "}
                              </option>
                              <option data-countrycode="MR" value="+222">
                                Mauritania{" "}
                              </option>
                              <option data-countrycode="YT" value="+269">
                                Mayotte{" "}
                              </option>
                              <option data-countrycode="MX" value="+52">
                                Mexico
                              </option>
                              <option data-countrycode="FM" value="+691">
                                Micronesia{" "}
                              </option>
                              <option data-countrycode="MD" value="+373">
                                Moldova{" "}
                              </option>
                              <option data-countrycode="MC" value="+377">
                                Monaco{" "}
                              </option>
                              <option data-countrycode="MN" value="+976">
                                Mongolia{" "}
                              </option>
                              <option data-countrycode="MS" value="+1664">
                                Montserrat{" "}
                              </option>
                              <option data-countrycode="MA" value="+212">
                                Morocco{" "}
                              </option>
                              <option data-countrycode="MZ" value="+258">
                                Mozambique{" "}
                              </option>
                              <option data-countrycode="MN" value="+95">
                                Myanmar
                              </option>
                              <option data-countrycode="NA" value="+264">
                                Namibia{" "}
                              </option>
                              <option data-countrycode="NR" value="+674">
                                Nauru{" "}
                              </option>
                              <option data-countrycode="NP" value="+977">
                                Nepal{" "}
                              </option>
                              <option data-countrycode="NL" value="+31">
                                Netherlands
                              </option>
                              <option data-countrycode="NC" value="+687">
                                New Caledonia{" "}
                              </option>
                              <option data-countrycode="NZ" value="+64">
                                New Zealand{" "}
                              </option>
                              <option data-countrycode="NI" value="+505">
                                Nicaragua{" "}
                              </option>
                              <option data-countrycode="NE" value="+227">
                                Niger{" "}
                              </option>
                              <option data-countrycode="NG" value="+234">
                                Nigeria{" "}
                              </option>
                              <option data-countrycode="NU" value="+683">
                                Niue{" "}
                              </option>
                              <option data-countrycode="NF" value="+672">
                                Norfolk Islands{" "}
                              </option>
                              <option data-countrycode="NP" value="+670">
                                Northern Marianas{" "}
                              </option>
                              <option data-countrycode="NO" value="+47">
                                Norway{" "}
                              </option>
                              <option data-countrycode="OM" value="+968">
                                Oman{" "}
                              </option>
                              <option data-countrycode="PW" value="+680">
                                Palau{" "}
                              </option>
                              <option data-countrycode="PA" value="+507">
                                Panama{" "}
                              </option>
                              <option data-countrycode="PG" value="+675">
                                Papua New Guinea{" "}
                              </option>
                              <option data-countrycode="PY" value="+595">
                                Paraguay{" "}
                              </option>
                              <option data-countrycode="PE" value="+51">
                                Peru{" "}
                              </option>
                              <option data-countrycode="PH" value="+63">
                                Philippines{" "}
                              </option>
                              <option data-countrycode="PL" value="+48">
                                Poland{" "}
                              </option>
                              <option data-countrycode="PT" value="+351">
                                Portugal{" "}
                              </option>
                              <option data-countrycode="PR" value="+1787">
                                Puerto Rico 8
                              </option>
                              <option data-countrycode="QA" value="+974">
                                Qatar{" "}
                              </option>
                              <option data-countrycode="RE" value="+262">
                                Reunion{" "}
                              </option>
                              <option data-countrycode="RO" value="+40">
                                Romania
                              </option>
                              <option data-countrycode="RU" value="+7">
                                Russia
                              </option>
                              <option data-countrycode="RW" value="+250">
                                Rwanda{" "}
                              </option>
                              <option data-countrycode="SM" value="+378">
                                San Marino{" "}
                              </option>
                              <option data-countrycode="ST" value="+239">
                                Sao Tome &amp; Principe{" "}
                              </option>
                              <option data-countrycode="SA" value="+966">
                                Saudi Arabia{" "}
                              </option>
                              <option data-countrycode="SN" value="+221">
                                Senegal{" "}
                              </option>
                              <option data-countrycode="CS" value="+381">
                                Serbia{" "}
                              </option>
                              <option data-countrycode="SC" value="+248">
                                Seychelles{" "}
                              </option>
                              <option data-countrycode="SL" value="+232">
                                Sierra Leone{" "}
                              </option>
                              <option data-countrycode="SG" value="+65">
                                Singapore
                              </option>
                              <option data-countrycode="SK" value="+421">
                                Slovak Republic{" "}
                              </option>
                              <option data-countrycode="SI" value="+386">
                                Slovenia{" "}
                              </option>
                              <option data-countrycode="SB" value="+677">
                                Solomon Islands{" "}
                              </option>
                              <option data-countrycode="SO" value="+252">
                                Somalia{" "}
                              </option>
                              <option data-countrycode="ZA" value="+27">
                                South Africa
                              </option>
                              <option data-countrycode="ES" value="+34">
                                Spain
                              </option>
                              <option data-countrycode="LK" value="+94">
                                Sri Lanka
                              </option>
                              <option data-countrycode="SH" value="+290">
                                St. Helena{" "}
                              </option>
                              <option data-countrycode="KN" value="+1869">
                                St. Kitts{" "}
                              </option>
                              <option data-countrycode="SC" value="+1758">
                                St. Lucia{" "}
                              </option>
                              <option data-countrycode="SD" value="+249">
                                Sudan{" "}
                              </option>
                              <option data-countrycode="SR" value="+597">
                                Suriname{" "}
                              </option>
                              <option data-countrycode="SZ" value="+268">
                                Swaziland{" "}
                              </option>
                              <option data-countrycode="SE" value="+46">
                                Sweden{" "}
                              </option>
                              <option data-countrycode="CH" value="+41">
                                Switzerland{" "}
                              </option>
                              <option data-countrycode="SI" value="+963">
                                Syria{" "}
                              </option>
                              <option data-countrycode="TW" value="+886">
                                Taiwan{" "}
                              </option>
                              <option data-countrycode="TJ" value="+7">
                                Tajikstan{" "}
                              </option>
                              <option data-countrycode="TH" value="+66">
                                Thailand{" "}
                              </option>
                              <option data-countrycode="TG" value="+228">
                                Togo{" "}
                              </option>
                              <option data-countrycode="TO" value="+676">
                                Tonga{" "}
                              </option>
                              <option data-countrycode="TT" value="+1868">
                                Trinidad &amp; Tobago
                              </option>
                              <option data-countrycode="TN" value="+216">
                                Tunisia
                              </option>
                              <option data-countrycode="TR" value="+90">
                                Turkey
                              </option>
                              <option data-countrycode="TM" value="+7">
                                Turkmenista{" "}
                              </option>
                              <option data-countrycode="TM" value="+993">
                                Turkmenistan
                              </option>
                              <option data-countrycode="TC" value="+1649">
                                Turks &amp; Caicos Islands
                              </option>
                              <option data-countrycode="TV" value="+688">
                                Tuvalu
                              </option>
                              <option data-countrycode="UG" value="+256">
                                Uganda
                              </option>
                              {/* <!-- <option data-countrycode="GB" value="+44">UK (+44</option> --> */}
                              <option data-countrycode="UA" value="+380">
                                Ukrain
                              </option>
                              <option data-countrycode="AE" value="+971">
                                United Arab Emirates{" "}
                              </option>
                              <option data-countrycode="UY" value="+598">
                                Uruguay{" "}
                              </option>
                              {/* <!-- <option data-countrycode="US" value="+1">USA (+1)</option> --> */}
                              <option data-countrycode="UZ" value="+7">
                                Uzbekistan{" "}
                              </option>
                              <option data-countrycode="VU" value="+678">
                                Vanuatu
                              </option>
                              <option data-countrycode="VA" value="+379">
                                Vatican City{" "}
                              </option>
                              <option data-countrycode="VE" value="+58">
                                Venezuela{" "}
                              </option>
                              <option data-countrycode="VN" value="+84">
                                Vietnam{" "}
                              </option>
                              <option data-countrycode="VG" value="+84">
                                Virgin Islands - British
                              </option>
                              <option data-countrycode="VI" value="+84">
                                Virgin Islands - US{" "}
                              </option>
                              <option data-countrycode="WF" value="+681">
                                Wallis &amp; Futuna{" "}
                              </option>
                              <option data-countrycode="YE" value="+969">
                                Yemen (North)
                              </option>
                              <option data-countrycode="YE" value="+967">
                                Yemen (South)
                              </option>
                              <option data-countrycode="ZM" value="+260">
                                Zambia{" "}
                              </option>
                              <option data-countrycode="ZW" value="+263">
                                Zimbabwe{" "}
                              </option>
                            </SelectGroup>
                          </div>

                          <div className="row my-3">
                            <div className="col-4">
                              <input
                                type="text"
                                className="form-control"
                                id="myText"
                                placeholder=""
                                value={this.state.calling_code}
                                onChange={this.onChange}
                                required
                              />
                            </div>
                            <div className="col-8">
                              <TextInput
                                type="tel"
                                className="form-control"
                                id=""
                                name="sms_verify_phone"
                                placeholder="Phone Number"
                                onChange={this.onChange}
                                required
                                pattern="[0-9\-\(\) +]{0,20}"
                                errorMessage={{
                                  pattern: "Please enter a valid number"
                                }}
                              />
                            </div>
                          </div>
                          <div className="my-3">
                            <TextInput
                              name="email"
                              id="email"
                              type="email"
                              placeholder="Email Address"
                              validator={validator.isEmail}
                              errorMessage={{
                                validator: "Please enter a valid email"
                              }}
                              value={this.state.email}
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="my-3">
                            <TextInput
                              name="password"
                              id="password"
                              type="password"
                              required
                              placeholder="Password"
                              pattern=".{6,}"
                              errorMessage={{
                                required: "Password is required",
                                pattern:
                                  "Password should be at least 6 characters"
                              }}
                              value={this.state.password}
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="mt-3">
                            <TextInput
                              name="password2"
                              id="password2"
                              type="password"
                              required
                              placeholder="Confirm Password"
                              validator={this.matchPassword}
                              errorMessage={{
                                required: "Confirm password is required",
                                validator: "Password does not match"
                              }}
                              value={this.state.confirmPassword}
                              onChange={this.onChange}
                            />
                          </div>

                          <div className="d-flex justify-content-start form_color">
                            <div className="checkbox">
                              <label
                                className="py-2"
                                style={{ fontSize: "12px" }}
                              >
                                <Checkbox
                                  name="tos"
                                  id="check1"
                                  required
                                  errorMessage="Please check this box"
                                  value={this.state.tos}
                                  required
                                  onChange={this.onChange}
                                />
                                I agree to the{" "}
                                <a
                                  href="/tos"
                                  target="_blank"
                                  className="text-light redirect"
                                >
                                  Terms and Conditions
                                </a>
                              </label>
                            </div>
                          </div>
                          <div className="form-group">
                            <button
                              className="btn btn-block login_btn"
                              style={{ fontSize: "14px" }}
                              type="submit"
                            >
                              <i className="fas fa-user text-dark" /> JOIN
                              EMPLUG NOW
                            </button>
                          </div>

                          {/* </form> */}
                        </ValidationForm>
                        <div>
                          <p
                            style={{ fontSize: "12px" }}
                            className="py-0 d-flex justify-content-end"
                          >
                            have an account?
                            <a
                              href="#"
                              className="form_color px-2 py-0 text-light redirect"
                              onClick={this.changeBackToLogin}
                            >
                              Click to Login
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div />
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="homeContent">
          <div className="container-fluid">
            <div className="row">
              <div className="col-md-6 plugs">
                <h2> Everyone just loves plugging...</h2>
                <p className="text-justify">
                  Emplug is a social enterprise, pioneering the world's first
                  global hub for integrated democratized job access, through its
                  innovative and low cost technology, members of the Emplug
                  community are connected to its solutions including the
                  underserved informal sector players in remote locations with
                  little or no internet access.
                </p>
                <button
                  type="button"
                  className="btn btn-outline-success btn-lg btn_color rounded-0"
                >
                  Get Plugged Now!
                </button>
              </div>
              <div className="col-md-6 opportunities">
                <div className="table-responsive">
                  <table className="table table-sm ">
                    <thead>
                      <tr className="bg-success py-3">
                        <th scope="" className="lemon" />
                        <th scope="" className="lemon">
                          Opportunities
                        </th>
                        <th scope="" className="darkGreen">
                          Industry
                        </th>
                        <th scope="" className="darkGreen">
                          Location
                        </th>
                        <th scope="" className="darkGreen">
                          Posted
                        </th>
                      </tr>
                    </thead>
                    <tbody id="myTable" />
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="cards" id="full">
          <div
            id="carouselExampleIndicators"
            className="carousel slide container"
            data-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <div className="row cardItems ">
                  <div className="col-md-6">
                    <img src={EmCard} />
                  </div>
                  <div className="col-md-6">
                    <h3>To Register on EMCARD</h3>
                    <p>
                      To register, text information in format below to 32811
                    </p>
                    <p>EMCARD(space)PIN*Employer*Industry*Location*email</p>
                    <p>
                      e.g.:
                      <br /> EMCARD 3654918725*Eloho
                      Eateries*Catering*Jos*info@eloho.com.ng
                    </p>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row cardItems">
                  <div className="col-md-6">
                    <img src={JobCard} />
                  </div>
                  <div className="col-md-6">
                    <h3>To Register on JOBCARD</h3>
                    <p>
                      To register, text information in format below to 32811
                    </p>
                    <p>
                      JOBCARD(space)PIN*Name*Current
                      Location*Profession*Gender*State of Origin*L.G.A*email
                      e.g.: <br /> JOBCARD 5618306541*Uche
                      Musa*Ibadan*Accountant*Male*Bauchi*Ningi*uchemusa@yahoo.com
                    </p>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row cardItems">
                  <div className="col-md-6">
                    <img src={AdCard} />
                  </div>
                  <div className="col-md-6">
                    <h3>To Register on ADCARD</h3>
                    <p>
                      ADCARD(space)PIN*Company*Location*Industry*email e.g.:
                      <br /> ADCARD 3814629025*JJ
                      Plc*Lagos*Construction*jjplc@yahoo.com
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="phone">
          <div className="container-fluid">
            <div className="row text-center">
              <div className="col-md-6 leftImg">
                <h3>Plug via SMS/USSD</h3>
                <img src={Coming} className="img-fluid" />
                <p>
                  No internet connection? No smartphone? <br /> No airtime? No
                  problem.
                </p>
              </div>
              <div className="col-md-6 compare">
                <img src={Compare} className="img-fluid" />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default Home;
