import React from "react";
import Navbar from "../Navbar/navbar";
import { HashRouter, Link } from "react-router-dom";

// Imports Images
import "../../css/team.css";
import TeamImg from "../../images/avatar.jpg";

const Team = () => {
  return (
    <div>
      <Navbar />

      <div className="bg_up">
        <div className="bg-top">
          <div className="teamLinks container">
            <Link class="p-2 text-muted" to="/about">
              About Us
            </Link>{" "}
            |
            <Link class="p-2 text-muted" to="/team">
              Team
            </Link>
          </div>
          <h3 className="text-center">MEET THE TEAM</h3>
        </div>
        <div className="bg_container">
          <div>
            <div className="grid">
              <figure className="">
                <figcaption>
                  <div className="text-center board">
                    <h3>
                      ADVISORY <br />
                      BOARD
                    </h3>
                  </div>
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quaenam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit.
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READMORE..
                      </a>
                    </span>
                  </p>

                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit...
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
            </div>
            <div className="grid">
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
            </div>
          </div>

          <div className="py-5">
            <div className="grid">
              <figure className="">
                <figcaption>
                  <div className="text-center board">
                    <h3>
                      BOARD OF <br />
                      DIRECTORS
                    </h3>
                  </div>
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quaenam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit.
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READMORE..
                      </a>
                    </span>
                  </p>

                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit...
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
            </div>
            <div className="grid">
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
            </div>
          </div>

          <div>
            <div className="grid">
              <figure className="">
                <figcaption>
                  <div className="text-center board">
                    <h3>MANAGEMENT</h3>
                  </div>
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quaenam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit.
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READMORE..
                      </a>
                    </span>
                  </p>

                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit...
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
            </div>
            <div className="grid">
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
              <figure className="effect-goliath">
                <img src={TeamImg} alt="Board Member" />
                <figcaption>
                  <div className="goliath_text  text-justify">
                    <h3>Jane Doe</h3>
                    <span className="">Chairman &amp; Investor</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Accusantium tempora dolor quae nam beatae? Quasi quia
                    accusamus sequi, eum in alias impedit..
                    <br />
                    <span className="text-primary font-weight-bold text_color">
                      <a data-toggle="modal" data-target="#squarespaceModal">
                        <i className="fas fa-plus text-primary" />
                        READ MORE..
                      </a>
                    </span>
                  </p>
                  {/* <!-- <a data-toggle="modal" data-target="#squarespaceModal">View more</a> --> */}
                </figcaption>
              </figure>
            </div>
          </div>
        </div>
      </div>

      {/* <!-- line modal --> */}
      <div
        class="modal fade"
        id="squarespaceModal"
        tabindex="-1"
        role="dialog"
        aria-labelledby="modalLabel"
        aria-hidden="true"
      >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
              </button>
              <h3 class="modal-title" id="lineModalLabel">
                My Modal
              </h3>
            </div>
            <div class="modal-body">
              {/* <!-- content goes here --> */}
              <form>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email address</label>
                  <input
                    type="email"
                    class="form-control"
                    id="exampleInputEmail1"
                    placeholder="Enter email"
                  />
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input
                    type="password"
                    class="form-control"
                    id="exampleInputPassword1"
                    placeholder="Password"
                  />
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input type="file" id="exampleInputFile" />
                  <p class="help-block">Example block-level help text here.</p>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" /> Check me out
                  </label>
                </div>
                <button type="submit" class="btn btn-default">
                  Submit
                </button>
              </form>
            </div>
            <div class="modal-footer">
              <div
                class="btn-group btn-group-justified"
                role="group"
                aria-label="group button"
              >
                <div class="btn-group" role="group">
                  <button
                    type="button"
                    class="btn btn-default"
                    data-dismiss="modal"
                    role="button"
                  >
                    Close
                  </button>
                </div>
                <div class="btn-group btn-delete hidden" role="group">
                  <button
                    type="button"
                    id="delImage"
                    class="btn btn-default btn-hover-red"
                    data-dismiss="modal"
                    role="button"
                  >
                    Delete
                  </button>
                </div>
                <div class="btn-group" role="group">
                  <button
                    type="button"
                    id="saveImage"
                    class="btn btn-default btn-hover-green"
                    data-action="save"
                    role="button"
                  >
                    Save
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{ height: "2580px" }} />
    </div>
  );
};
export default Team;
