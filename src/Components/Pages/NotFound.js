import React from "react";
import { Link } from "react-router-dom";

import Navbar from "../Navbar/navbar";

const NotFound = () => {
  return (
    <div>
      <Navbar />
      <div className="notFound text-center" style={{ marginTop: "3.5rem" }}>
        <h1>
          <span>404!</span>
          <br />
          Page Not Found
        </h1>
        <div className="bottomFix">
          Your Options
          <div className="flex py-4">
            <Link to="/" className="btn btn-secondary px-5">
              Go back home
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
