import React, { Component } from "react";
import { HashRouter, Link } from "react-router-dom";
import Navbar from "../Navbar/navbar";

// Imports Images
import AdCard from "../../images/adcard.png";
import EmCard from "../../images/emcard.png";
import JobCard from "../../images/jobcard.png";
import ComingSoon from "../../images/coming_soon.jpg";

//Material
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import NoSsr from "@material-ui/core/NoSsr";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired
};

function LinkTab(props) {
  return (
    <Tab component="a" onClick={event => event.preventDefault()} {...props} />
  );
}

const styles = theme => ({
  root: {
    flexGrow: 1
    // backgroundColor: theme.palette.background.paper,
  }
});

class About extends Component {
  state = {
    value: 0
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <about>
          <Navbar />

        <div class="title-bar" id="#">
          <div class="container">
            <nav class="nav d-flex ">
              <div class="col-12">
                <h3>Company</h3>
              </div>
              <p class="px-1">
                <Link class="p-2 text-muted" to="/about">
                  About Us
                </Link>{" "}
                |
                <Link class="p-2 text-muted" to="/team">
                  Team
                </Link>
              </p>
            </nav>
          </div>
        </div>

        <section className="about-us">
          <div className="container">
            <div className="row">
              <div className="col-12 text-center">
                <h1>Who We Are</h1>
                <p>
                  Emplug is a social enterprise, pioneering the world's first
                  global hub for integrated democratized job access,
                  collaborative entrepreneurial support services and a unique
                  process of integrating the informal sector into the formal
                  economy. Emplug, leverages the power of technology to
                  facilitate its processes. Through its innovative and low cost
                  services, members of the Emplug community are connected to its
                  solutions including the underserved informal sector players in
                  remote locations with little or no internet access.{" "}
                </p>

                <p>
                  At Emplug, we recognize that the informal sector makes up a
                  significant portion of the world workforce (61%), even though
                  it is often characterized as difficult and uncontrollable.
                  This enormous sector provides critical economic opportunities
                  for the underserved members of the society. Given the
                  importance of these critical players, Emplug facilitates the
                  integration of the informal economy into the formal sector
                  working through a multi-stakeholder partnership strategy. As a
                  result, releases the enormous potential of the sector for all
                  stakeholders thereby creating a win-win scenario for the
                  government and other players in the economy.
                </p>
                <p>
                  Given our belief that enterprise and society can be mutually
                  reinforcing, Emplug facilitates the implementation of social
                  causes in line with its vision, geared towards creating shared
                  value and sustainable home-grown solutions in underserved
                  communities.
                </p>
              </div>
            </div>
          </div>
        </section>

        <section className="mission">
          <div className="text-center py-5">
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <h1>
                    Our
                    <br /> Mision
                  </h1>
                </div>
                <div className="col-md-6 ">
                  <p className="text-justify">
                    We are committed to curbing global unemployment in the
                    formal and informal economic sectors, by making our platform
                    and solutions easy and affordable to all especially the
                    informal sector, with or without internet access or
                    facility.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="vision">
          <div className="text-center py-5">
            <div className="container">
              <div className="row">
                <div className="col-md-6">
                  <h1>
                    Our
                    <br /> Vision
                  </h1>
                </div>
                <div className="col-md-6 mt-3 ">
                  <p className="text-justify">
                    Our Vision is to build a working nation where everyone is a
                    partner and where the informal sector can be mainstreamed
                    into the economy.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="parallax">
          <div className="row text-white d-flex justify-content-center ml-5">
            <div className="col-md-8 p-5 ">
              <h3>How it works</h3>
              <p className="text-justify">
                Emplug empowers everyone to plug to businesses, employment
                opportunities, services, friends and family from socializing and
                chatting with friends to getting job calls and alerts from
                employees. Emplug simply grows your business, whether you have
                access to the internet or not. Emplug works all the time serving
                as a constant service community for you to make money from
                tasks. The Emplug platform is built with membership in mind for
                internet users and non-internet users, providing fast access to
                real time job seeking, hiring, advertising and service
                provisions without any internet connection.
              </p>
            </div>
          </div>
        </div>
        <section id="tabs">
          <div className="text-center">
            <h3>Membership is categorized as follows</h3>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-12">
                <NoSsr>
                  <div className={classes.root}>
                    <Tabs
                      variant="fullWidth"
                      value={value}
                      onChange={this.handleChange}
                    >
                      <LinkTab
                        label="JobCard"
                        href="page1"
                        className="Font-weigth-bold"
                        style={{ fontSize: "20px", color: "#232323" }}
                      />
                      <LinkTab
                        label="EMCard"
                        href="page2"
                        className="Font-weigth-bold"
                        style={{ fontSize: "20px", color: "#232323" }}
                      />
                      <LinkTab
                        label="AdCard"
                        href="page3"
                        className="Font-weigth-bold"
                        style={{ fontSize: "20px", color: "#232323" }}
                      />
                    </Tabs>
                    {value === 0 && (
                      <TabContainer className="px-3 px-sm-0">
                        <div className="row">
                          <div className="col-md-5 col-xs-12">
                            <img src={JobCard} />
                          </div>
                          <div className="col-md-7 col-xs-12 float-right mt-5">
                            <h4>Job Card</h4>
                            <p>
                              This is a membership category card which can be
                              obtained online or physically at $1/Annum
                            </p>

                            <p>For Whom?</p>
                            <ul>
                              <li>The Job Seeker</li>
                              <li>The Entrepreneur</li>
                              <li>Skilled Persons (e.g. Artisans)</li>
                              <li>Talented Individuals</li>
                            </ul>

                            <p>Membership Privileges:</p>
                            <ul>
                              <li>
                                Access to relevant information on employment
                                opportunities
                              </li>
                              <li>
                                Free entrepreneurial development trainings and
                                seminars
                              </li>
                              <li>
                                Admissions to job fair and quarterly luncheon
                              </li>
                              <li>
                                Input academic qualifications and skill details
                                on portal for employment opportunities
                              </li>
                              <li>View available employers</li>
                              <li>
                                Receive recruitment invitation from preferred
                                employers
                              </li>
                              <li>Submit business ideas for funding</li>
                              <li>
                                Instant Job offers in EMPLUG job Plugbay Centers
                              </li>
                              <li>
                                Access to online resources, tools or materials
                                for entrepreneurial development.
                              </li>
                            </ul>
                          </div>

                          <div className="col-md-5 col-xs-12 contentTab">
                            <div className="pr-2 pl-2">
                              <p>
                                JOBCARD(space)PIN*Name*Current
                                Location*Profession*Gender*State of
                                Origin*L.G.A*email
                                <br /> Send to 32810
                              </p>
                              <hr />
                              <p>
                                e.g.: <br /> JOBCARD 5618306541*Uche
                                Musa*Ibadan*Accountant*Male*Bauchi*Ningi*uchemusa@yahoo.com
                              </p>
                            </div>
                          </div>
                        </div>
                      </TabContainer>
                    )}
                    {value === 1 && (
                      <TabContainer className="px-3 px-sm-0">
                        <div className="row">
                          <div className="col-md-5 col-xs-12">
                            <img src={EmCard} />
                          </div>
                          <div className="col-md-7 col-xs-12 float-right mt-5">
                            <h4>EM Card</h4>
                            <p>
                              This membership category card can also be obtained
                              online or physically at the rate of $2/Annum
                            </p>

                            <p>For Whom?</p>
                            <ul>
                              <li>The Job Seeker</li>
                              <li>The Business Owner</li>
                              <li>Anyone who requires a service provider</li>
                            </ul>

                            <p>Membership Privileges:</p>
                            <ul>
                              <li>
                                Request for qualified workers or service
                                providers in your locality via SMS or the Web
                              </li>
                              <li>
                                Receive instant response via SMS or on Web wall
                              </li>
                              <li>
                                Access registered unemployed formal or informal
                                professionals(based on preference) in choice
                                location
                              </li>
                              <li>
                                Attend free Seminars on relevant business trends
                                and developments
                              </li>
                              <li>
                                Access to Business Applications and Human
                                Resource Tools
                              </li>
                              <li>
                                View Academic qualifications or skill details of
                                job seekers on Emplug platform
                              </li>
                              <li>Post Jobs and invite preferred candidates</li>
                              <li>Conduct online aptitude test at no cost</li>
                              <li>Own a homepage.</li>
                            </ul>
                          </div>

                          <div className="col-md-5 col-xs-12 contentTab">
                            <div className="pl-1">
                              <p>
                                To register EMCARD TEXT:
                                EMCARD(space)PIN*Employer*Industry*Location*email
                                Send to 33810
                              </p>
                              <hr />
                              <p>
                                e.g.: <br /> EMCARD 3654918725*Eloho
                                Eateries*Catering*Jos*info@eloho.com.ng
                              </p>
                            </div>
                          </div>
                        </div>
                      </TabContainer>
                    )}
                    {value === 2 && (
                      <TabContainer className="px-3 px-sm-0">
                        <div className="row">
                          <div className="col-md-5 col-xs-12">
                            <img src={AdCard} />
                          </div>
                          <div className="col-md-7 col-xs-12 float-right mt-5">
                            <h4>AD Card</h4>
                            <p>
                              This is a membership category card that can be
                              obtained online or physically at the rate of
                              $4/Annum.
                            </p>

                            <p>For Whom?</p>
                            <ul>
                              <li>The Advertiser</li>
                              <li>Product or Business Owner</li>
                              <li>Agent or Business Representative</li>
                            </ul>

                            <p>Membership Privileges:</p>
                            <ul>
                              <li>
                                Advertise new products or services to targeted
                                customers
                              </li>
                              <li>
                                Attend free Seminars on relevant business trends
                                and developments
                              </li>
                              <li>
                                Opportunity to have an online presence for your
                                product or services
                              </li>
                              <li>Send limitless number of adverts via SMS</li>
                              <li>
                                Own a home page that allows visitors to view
                                business profile,activitiesand contact details
                              </li>
                              <li>
                                Reach a large number of potential customers or
                                clients with new product or service details at
                                no cost.
                              </li>
                              <li>Send limitless number of adverts via SMS</li>
                              <li>
                                Opportunity to reach a wider audience with your
                                goods or services.
                              </li>
                            </ul>
                          </div>

                          <div className="col-md-5 col-xs-12 contentTab">
                            <div className="pl-2 pr-2">
                              <p>
                                To register ADCARD TEXT:
                                ADCARD(space)PIN*Company*Location*Industry*email
                                Send to 33810
                              </p>
                              <hr />
                              <p>
                                e.g.: <br /> ADCARD 3814629025*JJ
                                Plc*Lagos*Construction*jjplc@yahoo.com
                              </p>
                            </div>
                          </div>
                        </div>
                      </TabContainer>
                    )}
                  </div>
                </NoSsr>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-8 offset-md-2 col-sm-12">
                <p>
                  The JobCard, EmCard and AdCards can be acquired at any Zenith
                  Bank branch nationwide or with an Emplug Vending Partner (EVP)
                  near your location.
                </p>
                <p>
                  Bacome an Emplug Vending Partner (EVP) seize the opportunity
                  to have your own business with unique products and attractive
                  returns. <a href="">Click here for more information.</a>
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="success">
          <div className="container">
            <div className="row py-5 text-center">
              <div className="col-md-8 offset-md-2">
                <iframe
                  height="450px"
                  width="100%"
                  src="https://www.youtube.com/embed/BPw-4dqcPEs"
                  frameborder="0"
                  allow="autoplay; encrypted-media"
                  allowfullscreen
                />
              </div>
            </div>

            <div id="demo" className="carousel slide" data-ride="carousel">
              <h3>Success Stories</h3>
              <hr />

              <div className="row pb-4">
                <div className="col-md-4">
                  <img src={ComingSoon} />
                </div>
                <div className="col-md-4">
                  <img src={ComingSoon} />
                </div>
                <div className="col-md-4">
                  <img src={ComingSoon} />
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="parallax-feature">
          <div className="starfield" />
          <i className="fa fa-paper-plane" />
          <div className="feature-title">SUBSCRIBE TO OUR NEWSLETTER</div>
          <div className="feature-input">
            <div id="subscribe">
              <input
                type="text"
                className="enteremail error"
                name="EMAIL"
                id="subscribe-email"
                placeholder="Enter your email address"
                spellcheck="false"
              />
              <button
                type="submit"
                id="signup-button"
                className="signup-button"
              >
                Submit
              </button>
              <label for="subscribe-email" className="subscribe-message error">
                <i className="fa fa-warning" />
                <span className="message-content" />
              </label>
            </div>
          </div>
        </section>
      </about>
    );
  }
}
About.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(About);
