import React from "react";
import { HashRouter, Link } from "react-router-dom";

const Footer = () =>{
    return (
      <div>
        <HashRouter>
          <footer className="footer">
            <div className="container">
              <ul className="foote_bottom_ul_amrc pb-2">
                <li>
                  <Link to="/about">Company</Link>
                </li>

                {/* <!-- <li><Link to="team">Team</Link></li> --> */}
                <li>
                  <Link to="/programs">Programs</Link>
                </li>
                <li>
                  <Link to="/careers">Careers</Link>
                </li>
                <li>
                  <Link to="#" target="_blank">
                    Advertise
                  </Link>
                </li>
                <li>
                  <Link to="/events">Events</Link>
                </li>
                <li>
                  <Link to="#">Developers</Link>
                </li>
                <li>
                  <Link to="#">Marketplace</Link>
                </li>
                <li>
                  <Link to="/tos">Terms &amp; Policies</Link>
                </li>
                <li>
                  <a href="https://blog.emplug.com" target="_blank">
                    Blog &amp; Press Center
                  </a>
                </li>
                <li>
                  <Link to="#">Language</Link>
                </li>
              </ul>
              <div className="shadow_top" />
              {/* <!--foote_bottom_ul_amrc ends here--> */}
              <p className="text-center pt-2">
                &copy; Copyright 2018 | Emplug, Inc. All rights reserved
              </p>
            </div>
          </footer>
        </HashRouter>
      </div>
    );
}
export default Footer;
