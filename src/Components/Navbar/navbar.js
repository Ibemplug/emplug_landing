import React from "react";
import { HashRouter, Link } from "react-router-dom";
import Logo from "../../images/logo.png";

class Navbar extends React.Component {
  render() {
    return (
      <div>
        <div className="navbar navbar-dark fixed-top">
          <div className="container">
            <nav className="nav d-flex justify-content-between">
              <Link to="/" className="navbar-brand d-flex align-items-center">
                <img src={Logo} alt="emplug logo" />
              </Link>
            </nav>
          </div>
        </div>
      </div>
    );
  }
}
export default Navbar;
